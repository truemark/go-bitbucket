module bitbucket.org/truemark/go-bitbucket
go 1.16

require (
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e // indirect
	golang.org/x/oauth2 v0.0.0-20210628180205-a41e5a781914
)
